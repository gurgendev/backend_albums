## This is Backend Project

## Registration
POST 

/api/register

````
name
email
password
password_confirmation
````

## Login
POST 

/api/login

````
email
password
````

## Logout
GET 

/api/logout

````
token
````

## Get All Albums
GET 

/api/albums

````
token
````

## Get Album By Id
GET 

/api/albums/{id}

````
token
````

## Create Album
POST 

/api/albums

````
token
````

## Update Album
PUT 

/api/albums/{id}

````
token
````

## DELETE Album
DELETE
 
/api/albums/{id}

````
token
````

==================================
## ARTISAN

php artisan route:cache 

php artisan cache:clear 

php artisan config:cache 

php artisan view:clear 

== 
php artisan passport:install
