<?php

namespace App\Contracts;

interface AlbumsServiceInterface
{
    public function getAlbums();

    public function createAlbum($request);

    public function getAlbumById($id, $sort);

    public function updateAlbums($id, $request);

    public function deleteAlbums($id);
}
