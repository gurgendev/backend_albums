<?php

namespace App\Contracts;

interface AuthServiceInterface
{

    public function registerUser($request);

    public function loginUser();

}
