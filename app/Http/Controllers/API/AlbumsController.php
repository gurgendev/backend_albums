<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Contracts\AlbumsServiceInterface;
use App\Http\Requests\AlbumRequest;

class AlbumsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AlbumsServiceInterface $albumsService)
    {
        $success = $albumsService->getAlbums();
        return $this->sendResponse($success, 'Get all albums data successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->sendResponse('Closed Route', '404');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, AlbumsServiceInterface $albumsService, AlbumRequest $albumRequest)
    {
        $rules = $albumRequest->authorize();

        $success = $albumsService->createAlbum($request);
        return $this->sendResponse($success, 'Created album successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, AlbumsServiceInterface $albumsService)
    {
        $success = $albumsService->getAlbumById($id);
        return $this->sendResponse($success, 'Get single album successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->sendResponse('Closed Route', '404');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, AlbumsServiceInterface $albumsService, AlbumRequest $albumRequest)
    {
        $rules = $albumRequest->authorize();

        $success = $albumsService->updateAlbums($id, $request);
        return $this->sendResponse($success, 'Updated album successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, AlbumsServiceInterface $albumsService)
    {
        $success = $albumsService->deleteAlbums($id);
        return $this->sendResponse($success, 'Deleted album successfully.');
    }
    /**
     * Display a listing of the resource by sort.
     *
     * @return \Illuminate\Http\Response
     */
    public function sortList($id, $sort, AlbumsServiceInterface $albumsService)
    {
        $success = $albumsService->getAlbumById($id, $sort);
        return $this->sendResponse($success, 'Get all albums by sort successfully.');
    }

}
