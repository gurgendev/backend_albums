<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AlbumRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'album' => 'required|string|min:5|max:20',
        ];
    }

    public function messages()
    {
        return [
            'album.required' => __('Name is required'),
            'album.string' => __('Name must be a string'),
            'album.min' => __('Name must have a minimal of 5 characters'),
            'album.max' => __('Name must have a maximal of 20 characters'),
        ];
    }
}
