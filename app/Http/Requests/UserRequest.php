<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users,email',
            'name' => 'required|string|min:5|max:20',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => __('e-Mail is required'),
            'email.email' => __('e-Mail must be a valid address'),
            'email.unique' => __('This e-Mail is already registered'),
            'name.required' => __('Name is required'),
            'name.string' => __('Name must be a string'),
            'name.min' => __('Name must have a minimal of 5 characters'),
            'name.max' => __('Name must have a maximal of 20 characters'),
        ];
    }
}
