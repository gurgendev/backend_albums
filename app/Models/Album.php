<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $table = 'albums';

    protected $fillable = [
        'name',
        'user_id',
    ];

    /********************* Relations *******************/

    /**
     * Get the images for the album.
     */
    public function images()
    {
        return $this->hasMany('App\Models\Image');
    }

    /**
     * Get the user that owns the albums.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
