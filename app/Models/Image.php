<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';

    protected $fillable = [
        'name',
        'album_id',
    ];

    /********************* Relations *******************/

    /**
     * Get the album that owns the images.
     */
    public function album()
    {
        return $this->belongsTo('App\Models\Album');
    }

}
