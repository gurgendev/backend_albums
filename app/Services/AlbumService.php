<?php

namespace App\Services;

use App\Contracts\AlbumsServiceInterface;
use App\Models\Album;
use Illuminate\Support\Facades\Auth;

class AlbumService implements AlbumsServiceInterface
{
    private $albumModel;

    private $loginUser;

    public function __construct( Album $albumModel )
    {
        $this->albumModel = $albumModel;
        $this->loginUser = Auth::user();
    }

    /**
     * Get all albums by sort Service
     *
     */
    public function getAlbums()
    {
        return $this->albumModel->where('user_id', $this->loginUser->id)->with(['images' => function ($query) {
            $query->orderBy('id', 'desc');
        }])->get();
    }

    /**
     * Create album Service
     *
     */
    public function createAlbum($request)
    {
        $album = new Album;
        $album->name = $request->input('album');

        return $this->loginUser->albums()->save($album);
    }

    /**
     * Get album by id Service
     *
     */
    public function getAlbumById($id, $sort = 'desc')
    {
        return $this->albumModel->where('id', $id)->with(['images' => function ($query) use ($sort) {
            $query->orderBy('id', $sort);
        }])->get();
    }

    /**
     * Update album Service
     *
     */
    public function updateAlbums($id, $request)
    {
        $album = $this->albumModel->where('id', $id)->first();
        $album->name = $request->input('album');

        return $this->loginUser->albums()->save($album);
    }

    /**
     * Delete album Service
     *
     */
    public function deleteAlbums($id)
    {
        return $this->albumModel->where('id', $id)->delete();
    }
}
