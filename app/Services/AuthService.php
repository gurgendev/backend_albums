<?php

namespace App\Services;

use App\Contracts\AuthServiceInterface;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthService implements AuthServiceInterface
{
    private $tokenKey = 'TokenForAuthUserInApp';

    /**
     * Register Service
     *
     */
    public function registerUser($request)
    {

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken($this->tokenKey)->accessToken;
        $success['name'] = $user->name;
        $success['id'] = $user->id;

        return $success;
    }

    /**
     * Login Service
     *
     */
    public function loginUser()
    {

        $user = Auth::user();
        $success['token'] = $user->createToken($this->tokenKey)->accessToken;
        $success['name'] = $user->name;
        $success['id'] = $user->id;

        return $success;
    }
}
